#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <sys/iofunc.h>
#include <sys/dispatch.h>

#define RESOURCE_PATH "/dev/sensor"

char rnd_state[32];

int io_read(resmgr_context_t *ctp, io_read_t *msg, RESMGR_OCB_T *ocb);

int main(void) {
	resmgr_attr_t resmgr_attr;
	memset(&resmgr_attr, 0, sizeof(resmgr_attr));
	resmgr_attr.nparts_max = 1;
	resmgr_attr.msg_max_size = 2048;

	resmgr_connect_funcs_t connect_funcs;
	resmgr_io_funcs_t io_funcs;
	iofunc_func_init(_RESMGR_CONNECT_NFUNCS, &connect_funcs, _RESMGR_IO_NFUNCS, &io_funcs);
	io_funcs.read = io_read;

	iofunc_attr_t attr;
	iofunc_attr_init(&attr, S_IFNAM | 0666, 0, 0);

	dispatch_t *dpp = dispatch_create();
	if (!dpp) {
		perror("Unable to dispatch_create");
		exit(EXIT_FAILURE);
	}

	if (resmgr_attach(dpp, &resmgr_attr, RESOURCE_PATH, _FTYPE_ANY, 0, &connect_funcs, &io_funcs, &attr) == -1) {
		perror("Unable to resmgr_attach");
		exit(EXIT_FAILURE);
	}

	dispatch_context_t *ctp = dispatch_context_alloc(dpp);

	while (1) {
		ctp = dispatch_block(ctp);
		if (!ctp) {
			perror("Unable to dispatch_block");
			exit(EXIT_FAILURE);
		}

		dispatch_handler(ctp);
	}

	return EXIT_SUCCESS;
}

int sensor_reading = 0x5f;

char get_sensor_reading() {
    int n = rand();
    if ((n & 0xf) == 0xf)
        return 0xd9;
	int delta = (n & 0xf) - 0x8;
	sensor_reading += delta;
	if (sensor_reading < 0)
		sensor_reading = 0;
	else if (sensor_reading > 0xbf)
		sensor_reading = 0xbf;

	return sensor_reading;
}

int io_read(resmgr_context_t *ctp, io_read_t *msg, RESMGR_OCB_T *ocb) {
	int sts = iofunc_read_verify(ctp, msg, ocb, NULL);
	if (sts != EOK)
		return sts;

	char reading = get_sensor_reading();

	MsgReply(ctp->rcvid, 1, &reading, 1);

	ocb->attr->flags |= IOFUNC_ATTR_ATIME | IOFUNC_ATTR_DIRTY_TIME;

	return _RESMGR_NOREPLY;
}
