
#include "SensorValue.h"
#include <QGuiApplication>
#include <QtQuick>

int main( int argc, char * argv[] )
{
    QGuiApplication app(argc, argv);

    SensorValue sensorValue;

    QQuickView view;
    view.rootContext()->setContextProperty("sensorValue", &sensorValue);
    view.setSource(QUrl("qrc:///SensorMonitor.qml"));
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.showFullScreen();

    app.connect(view.engine(), SIGNAL(quit()), SLOT(quit()));

    return app.exec();
}
