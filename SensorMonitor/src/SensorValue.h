/*
 * SensorValue.h
 *
 *  Created on: Oct 8, 2014
 *      Author: dschaefer
 */

#ifndef SENSORVALUE_H_
#define SENSORVALUE_H_

#include <QObject>
#include <stdio.h>

class SensorValue : public QObject
{
Q_OBJECT

public:
    SensorValue();
    virtual ~SensorValue();

protected:
    virtual void timerEvent(QTimerEvent *);

signals:
    void changed(QString value);

private:
    FILE *sensor;
};

#endif /* SENSORVALUE_H_ */
