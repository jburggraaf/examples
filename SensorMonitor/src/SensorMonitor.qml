import QtQuick 2.0

Rectangle {
    id: parent
    color: "lightblue"

    MouseArea {
        anchors.fill: parent
        onClicked: {
        	Qt.quit()
       	}
    }

    Text {
        id: title
        text: "Sensor Monitor"
        font.family: "Helvetica"
        font.pointSize: 24
        anchors.centerIn: parent
    }

    Text {
        id: sensorDisplay
        objectName: "sensorDisplay"
        font.family: "Helvetica"
        font.pointSize: 16
        anchors {
            horizontalCenter: title.horizontalCenter
            top: title.bottom
        }

        Connections {
            target: sensorValue
            onChanged: {
                sensorDisplay.text = value
            }
        }
    }
}
