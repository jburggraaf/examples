/*
 * SensorValue.cpp
 *
 *  Created on: Oct 8, 2014
 *      Author: dschaefer
 */

#include <src/SensorValue.h>
#include <string.h>

SensorValue::SensorValue()
{
    // Open the sensor file
    sensor = fopen("/dev/sensor", "r");
    if (!sensor) {
        perror("failed to open sensor");
        exit(1);
    }

    startTimer(1000);
}

SensorValue::~SensorValue()
{
    fclose(sensor);
}

void SensorValue::timerEvent(QTimerEvent *) {
    int sensorReading = fgetc(sensor);

    const char * message;
    if (sensorReading < 64) {
        message = "Low";
    } else if (sensorReading < 128) {
        message = "Nominal";
    } else if (sensorReading < 196) {
        message = "High";
    } else {
        message = "Out of Range";
    }

    emit changed(QString::number(sensorReading) + " " + message);
}
