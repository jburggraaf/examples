QT       += qml quick
HEADERS   = src/SensorValue.h
SOURCES   = src/SensorValue.cpp src/SensorMonitor.cpp
RESOURCES = src/SensorMonitor.qrc
OTHER_FILES = src/SensorMonitor.qml
QMAKE_CXXFLAGS_RELEASE *= -g
