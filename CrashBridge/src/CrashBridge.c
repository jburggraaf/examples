#include <stdlib.h>
#include <stdio.h>

#include "../public/CrashBridge.h"
#include "../../crasher/public/crasher.h"

int shared_func()
{
   int result = run_crash();
   printf( "This is a function from shared lib\n" );
   return result;
}
