#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../../CrashBridge/public/CrashBridge.h"

int step2Path2() {
	return shared_func();
}

int step1Path2() {
	return step2Path2();
}

int step2Path1() {
	return shared_func();
}

int step1Path1() {
	return step2Path1();
}

int main(int argc, char *argv[]) {
    int res = 0;
    //volatile
//    res = staticFunc();
    if (argc > 1) {
        res = step1Path2();
    } else {
    	res = step1Path1();
    }

    return EXIT_SUCCESS;
}
