#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

void doWork() {
	printf("Hope this works: %s\n", NULL);
}

void doJob1() {
	doWork();
}

void doJob2() {
	doWork();
}

void doJob3() {
	char * msg = NULL;
	char c;
	for (c = *msg; c; c++) {
		printf("%d", c);
	}
}

void doJob4() {
	char * msg = NULL;
	char c;
	for (c = *msg; c; c++) {
		printf("%d", c);
	}
}

int main(void) {
	int fd = open("/dev/random", O_RDWR);
	if (fd == -1) {
		printf("Error opening /dev/random\n");
		return -1;
	}

	char byte;
	read(fd, &byte, sizeof(byte));
	int job = byte & 3;
	printf("job: %d\n", job);

	switch (job) {
	case 0:
		doJob1();
		break;
	case 1:
		doJob2();
		break;
	case 2:
		doJob3();
		break;
	case 3:
		doJob4();
		break;
	default:
		printf("weird\n", job);
	}
	return 0;
}
